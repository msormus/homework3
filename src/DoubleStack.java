
import java.util.Arrays;
import java.util.LinkedList;

//http://www.mycstutorials.com/articles/data_structures/stacks

public class DoubleStack {

	private LinkedList<Double> proov;

	public static void main(String[] argum) {
		DoubleStack doubleList = new DoubleStack();

		System.out.println(doubleList);
		doubleList.push(1);
		System.out.println(doubleList);
		doubleList.push(8);
		System.out.println(doubleList);
		doubleList.push(22);
		System.out.println(doubleList);
		doubleList.push(5);
		System.out.println(doubleList);
		doubleList.push(1);
		System.out.println(doubleList);
		doubleList.op("+");
		System.out.println(doubleList);
		doubleList.op("-");
		System.out.println(doubleList);
		doubleList.op("/");
		System.out.println(doubleList);
		doubleList.op("*");
		System.out.println(doubleList);
		System.out.println(DoubleStack.interpret("2 15 - "));

	}

	DoubleStack() {
		proov = new LinkedList<Double>();

	}

	@Override
	public Object clone() throws CloneNotSupportedException {

		DoubleStack koopia = new DoubleStack();
		koopia.proov = (LinkedList<Double>) proov.clone();

		return koopia;
	}

	public boolean stEmpty() {

		return proov.isEmpty();

	}

	public void push(double a) {
		proov.push(a);

	}

	public double pop() {

		if (this.stEmpty())
			throw new IndexOutOfBoundsException("Avaldis on tühi. Palun sisesta uuesti.");

		return proov.pop();

	}

	public void op(String s) {
		if (this.stEmpty())
			throw new IndexOutOfBoundsException("Avaldis on tühi. Palun sisesta uuesti.");

		if (this.proov.size() < 2) {
			throw new IllegalArgumentException(
					String.format("Avaldise tegemiseks on liiga v�he elemente. Viga on avaldises"));

		}

		if (s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {

			double op2 = pop();
			double op1 = pop();

			if (s.equals("+")) {
				push(op1 + op2);
			}

			if (s.equals("-")) {
				push(op1 - op2);
			}

			if (s.equals("*")) {
				push(op1 * op2);
			}

			if (s.equals("/")) {
				push(op1 / op2);
			}
		} else {

			throw new RuntimeException("Tehet " + s + " pole v�imalik teha. Kasuta +, -, * ja / tehteid. ");

		}

	}

	public double tos() {

		if (stEmpty())
			throw new IndexOutOfBoundsException("Avaldis on t�hi. Palun sisesta avaldis uuesti.");

		return proov.getFirst();

	}

	@Override
	public boolean equals(Object o) {
		if (proov.equals(((DoubleStack) o).proov))
			return true;

		else

			return false;

	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		for (int i = proov.size() - 1; i >= 0; i--) {

			sb.append(proov.get(i)).append(" ");

		}

		return sb.toString();

	}

	public static double interpret(String pol) {
		if (pol == null || pol.length() == 0) {

			throw new RuntimeException("Palun sisesta avaldis.");
		}

		else {

			DoubleStack stack = new DoubleStack(); // uus
			String[] elements = pol.trim().split("\\s+");
			int a = 0;
			int b = 0;

			for (String element : elements) {

				try {
					stack.push(Double.valueOf(element)); // Double element
															// lisatakse
															// magasini
					a++;

				} catch (NumberFormatException e) {

					if (stack.proov.size() < 2)

						throw new RuntimeException(
								"Avaldise tegemiseks on liiga vähe elemente. Viga on avaldises " + pol);

					else if (!element.equals("+") && !element.equals("-") && !element.equals("*")
							&& !element.equals("/"))

						throw new RuntimeException(pol + " avaldisse sisestatud '" + element
								+ "' tehe pole lubatud. Kasuta +, -, * ja / tehteid.");

					else if (stack.stEmpty())

						throw new RuntimeException("Palun lisa avaldis " + pol);

					stack.op(element);
					b++;
				}
			}

			if (a - 1 != b)
				throw new RuntimeException("Avaldist " + pol + " ei ole võimalik teostada.");
			return stack.pop();

		}
	}

}
